import Vue from 'vue'
import Router from 'vue-router'

import FullCanlender from 'fullcalendar';

const Layout = () => import('@/components/Layout')

const Canlender = () => import(/* webpackChunkName: "canlender" */ '@/components/canlender/Canlender')
const WorkBench = () => import(/* webpackChunkName: "WorkBench" */ '@/components/workBench/WorkBench')
const ProjectBD = () => import(/* webpackChunkName: "WorkBench" */ '@/components/workBench/ProjectBD')
const ProjectBDAdd = () => import(/* webpackChunkName: "WorkBench" */ '@/components/workBench/ProjectBDAdd')

const Login = () => import(/* webpackChunkName: "login" */ '@/components/Login')

const UserList = () => import(/* webpackChunkName: "group-user" */ '@/components/user/List')
const UserAdd = () => import(/* webpackChunkName: "group-user" */ '@/components/user/Add')
const UserEdit = () => import(/* webpackChunkName: "group-user" */ '@/components/user/Edit')
const UserChangePwd = () => import(/* webpackChunkName: "group-user" */ '@/components/user/ChangePwd')


Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Layout',
      component: Login,
      meta: {name: '首页'}
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {name: '登录'}
    },
    {
      path: '/canlender',
      component: Layout,
      children: [
        {
          path: '/canlender',
          component: Canlender,
          meta: {index: 2, name: '日程表', requiresAuth: true}
        }
      ]
    },
    {
      path: '/workBench',
      component: Layout,
      children: [
        {
          path: '/',
          component: ProjectBD,
          name: 'ProjectBD',
          meta: {index: 3, name: '工作台', requiresAuth: true}
        },
        {
          path: 'projectBDAdd',
          component: ProjectBDAdd,
          meta: {index: 3, name: '添加项目', requiresAuth: true}
        }
      ]
    },
    {
      path: '/user',
      redirect: '/user/list',
      component: Layout,
      meta: {name: '用户管理', requiresAuth: true},
      children: [
        {
          path: 'list',
          component: UserList,
          meta: {name: '用户列表', requiresAuth: true}
        },
        {
          path: 'add',
          component: UserAdd,
          meta: {name: '添加用户', requiresAuth: true}
        },
        {
          path: 'edit_:id',
          component: UserEdit,
          meta: {name: '编辑用户', requiresAuth: true}
        },
        {
          path: 'changePwd',
          component: UserChangePwd,
          meta: {name: '修改密码', requiresAuth: true}
        }
      ]
    }
  ]
})

export default router
