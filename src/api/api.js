import axios from 'axios';

const host = location.host
var _root = (host.indexOf('localhost') > -1 || host.indexOf('47.92.54.162') > -1) ? 'http://47.92.54.162:8081' : '/index.php';
var _root = 'http://fa.hupogu.com/index.php';

axios.interceptors.request.use(function (config) {
  /*if (config.method = 'post') {
    config.headers['Content-Type'] = 'multipart/form-data'
  }*/
  return config
})
//添加一个响应拦截器
axios.interceptors.response.use(function (res) {
  //在这里对返回的数据进行处理
  return res;
}, function (err) {
  //Do something with response error
  return Promise.reject(err);
})
//全局
//文件上传
export const fileuUploadUrl = `${_root}/Attach/doUpload`;
export const fileuUpload = params => { return axios.post(`${_root}/Attach/doUpload`, params).then(res => res.data); };
export const loginInfo = params => { return axios.post(`${_root}/User/loginInfo`, params).then(res => res.data); };
export const loginOut = params => { return axios.post(`${_root}/User/loginOut`, params).then(res => res.data); };
export const getSelectData = params => { return axios.get(`${_root}/Project/getSelectData`, params).then(res => res.data); };

//用户相关接口
// 用户登录
export const login = params => { return axios.post(`${_root}/login`, params).then(res => res.data); };
export const getMembers = params => { return axios.post(`${_root}/User/search`, params).then(res => res.data); };

/*项目管理接口*/
export const getProject = params => { return axios.get(`${_root}/Project/addBaseInfo`, params).then(res => res.data); }; //id
export const addEditProject = params => { return axios.post(`${_root}/Project/addBaseInfo`, params).then(res => res.data); };
export const listProject = params => { return axios.get(`${_root}/Project/pageList`, params).then(res => res.data); };
export const getAuthManage = params => { return axios.get(`${_root}/Project/authManage`, params).then(res => res.data); };
export const addEditAuthManage = params => { return axios.post(`${_root}/Project/authManage`, params).then(res => res.data); };
export const getMarketCom = params => { return axios.get(`${_root}/Project/marketCom`, params).then(res => res.data); };
export const addEditMarketCom = params => { return axios.post(`${_root}/Project/marketCom`, params).then(res => res.data); };
export const getRoadShow = params => { return axios.get(`${_root}/Project/roadShow`, params).then(res => res.data); };
export const addEditRoadShow = params => { return axios.post(`${_root}/Project/roadShow`, params).then(res => res.data); };
export const insPageList = params => { return axios.get(`${_root}/Invest/insPageList`, params).then(res => res.data); };
export const addIns = params => { return axios.post(`${_root}/Project/addIns`, params).then(res => res.data); };
export const deleteIns = params => { return axios.get(`${_root}/Project/deleteIns`, params).then(res => res.data); };
export const investorPageList = params => { return axios.get(`${_root}/Project/investorPageList`, params).then(res => res.data); };
export const updateIns = params => { return axios.post(`${_root}/Project/updateIns`, params).then(res => res.data); };

export const getDeliveryAgree = params => { return axios.get(`${_root}/Project/deliveryAgree`, params).then(res => res.data); };
export const updateDeliveryAgree = params => { return axios.post(`${_root}/Project/deliveryAgree`, params).then(res => res.data); };
export const feedBack = params => { return axios.get(`${_root}/Project/feedBack`, params).then(res => res.data); };
export const updateFeedBack = params => { return axios.post(`${_root}/Project/updateFeedBack`, params).then(res => res.data); };
export const upFeedBackStatus = params => { return axios.post(`${_root}/Project/upFeedBackStatus`, params).then(res => res.data); };

export const getProjectsList = params => { return axios.get(`${_root}/ProjectFollows/getList`, params).then(res => res.data); };
export const delProject = params => { return axios.get(`${_root}/ProjectFollows/delProject`, params).then(res => res.data); };

/*日程管理接口*/
export const addEditSchedule = params => { return axios.post(`${_root}/Schedule/add`, params).then(res => res.data); };
export const getScheduleList = params => { return axios.get(`${_root}/Schedule/index`, params).then(res => res.data); };
export const getScheduleDetail = params => { return axios.get(`${_root}/Schedule/detail`, params).then(res => res.data); };
export const delSchedule = params => { return axios.get(`${_root}/Schedule/delete`, params).then(res => res.data); };
export const eventManage = params => { return axios.get(`${_root}/Schedule/add`, params).then(res => res.data); };
export const joinStatus = params => { return axios.get(`${_root}/Schedule/joinStatus`, params).then(res => res.data); };

//认证管理
export const authList = params => { return axios.get(`${_root}/Admin/authList`, params).then(res => res.data); };
export const doAuth = params => { return axios.get(`${_root}/Admin/doAuth`, params).then(res => res.data); };
export const applyList = params => { return axios.get(`${_root}/Admin/applyList`, params).then(res => res.data); };
export const doApply = params => { return axios.get(`${_root}/Admin/doApply`, params).then(res => res.data); };
//export const eventManage = params => { return axios.get(`${_root}/Schedule/add`, params).then(res => res.data); };

/*
const mixins = function (fn) {
  return new Promise((resolve, reject) => {
    fn.then(res => {
      var data = res.body
      if (data.errno == 0) {
        resolve(res.body)
      } else {
        reject(res.body)
      }
    }).catch(err => {
      reject(err)
    })
  })
}

const userGetCurrent = function ({username, password}) {
  return mixins(Vue.http.post("/auth/userGetCurrent", {username, password}))
}
const getInfoById = function ({id}) {
  return {
    visible: false,
    id: 10,
    name: 'test',
    selProject: 1,
    selMembers: [1],
    date1: '2018-03-07 14:00:00',
    date2: '2018-03-07 19:00:00',
    addr: 'addr',
    desc: 'desc'
  }
  return mixins(Vue.http.post("/auth/userGetCurrent", {id}))
}

*/
export default {
  login
}
