import Vue from 'vue'
import VueResource from 'vue-resource'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import '@/assets/element-variables.scss'
import App from './App'

Vue.config.productionTip = false

Vue.use(ElementUI, {size: 'small'})
Vue.use(VueResource)

router.onReady(() => {
  // 打开页面时检查是否登录
  store.dispatch('getUser')
  store.dispatch('getSelectData')
})

var app = new Vue({
  el: '#app',
  router,
  store,
  components: {App},
  template: '<App/>',
  beforeCreate() {
    /*Vue.http.interceptors.push((req) => {
      return (res) => {
        if (res.status == 200) { // 业务类错误
          if (res.body.errno == '10001') { //用户未登录
            store.commit('setUser', {}); //清理sessionStorage
            router.replace({
              path: '/login',
              // 获取登录后的跳转路径
              query: { redirect: app.$route.path == '/login' ? app.$route.query.redirect : app.$route.fullPath }
            })
          }
          if (res.body.errno == '2') { //校验错误
            app.$message.error(res.body.errmsg);
          }
        } else {
          app.$notify.error({
            title: '错误' + res.status,
            message: res.status ? res.statusText : '网络错误！'
          });
        }
      };
    });*/
    router.beforeEach((to, from, next) => {
      // 检查是否登录，根据router中配置的meta:requiresAuth项决定是否需要判断
      if (to.matched.some(record => record.meta.requiresAuth)) {
        store.dispatch('getUser').then(user => {
          console.log('beforeEach:', user);
          if (user && user.uid) {
            next()
          } else {
            next({
              path: '/login',
              query: {redirect: to.fullPath}
            })
          }
        })
      } else {
        next()
      }
    })
  }
})
