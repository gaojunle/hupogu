import Vue from 'vue'
import Vuex from 'vuex'

import {loginInfo, getSelectData} from "@/api/api";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    selectData: {}
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    setSelectData(state, selectData) {
      state.selectData = selectData
    }
  },
  actions: {
    getUser({state, commit}) {
      //先查看store里的user，再发起请求去服务器查询，此处存在问题是，如果第二个页面退出了，但是第一个页面不知道，只有接口请求才会判断是否登录。
      return new Promise((resolve, reject) => {
        let user = state.user
        if (user && user.uid) {
          resolve(user)
        } else {
          loginInfo().then(ret => {
            if (ret.errorno == '-2') {
              commit('setUser', null);
              resolve(user)
            } else {
              let user = ret.data;
              commit('setUser', user)
              resolve(user)
            }
          }).catch(err => {
            commit('setUser', null)
            reject(err)
          })
        }
      })
    },
    getSelectData({state, commit}) {
      return new Promise((resolve, reject) => {
        getSelectData().then(ret => {
          if (ret.errorno == 0) {

            var selectData = {};
            for (var item in ret.data) {
              selectData[item] = [];
              var _obj = ret.data[item];
              for (var key in _obj) {
                selectData[item].push({status: _obj[key], key: key})
              }
            }

            commit('setSelectData', selectData);
          }
        })
      })
    }
  }
})
